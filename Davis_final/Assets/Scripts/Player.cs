﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public bool invincible = false;


    [System.Serializable]
    public class PlayerStats
    {
        public int maxHealth = 100;


        private int _curHealth;
        public int curHealth
        {
            get { return _curHealth; }
            set { _curHealth = Mathf.Clamp(value, 0, maxHealth); }
        }

        public void Init()
        {
            curHealth = maxHealth;
        }
    }

    public PlayerStats stats = new PlayerStats();

    public int fallBoundary = -20;

    [SerializeField]
    private StatusIndicator statusIndicator;

    void Start()
    {
        if (statusIndicator == null)
        {
            Debug.LogError("No status indicator referenced on player");

        }
        else
        {
            stats.curHealth = stats.maxHealth;
            statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
        }
    }

    void Update()
    {
        if (transform.position.y <= fallBoundary)
        {
            DamagePlayer(99999999);
        }
    }

    public void DamagePlayer (int damage)
    {
        if (stats.curHealth > 0 && !invincible)
        {
            invincible = true;
            Invoke("EndInvincible", 1f);
            stats.curHealth -= damage;
            statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
        }
        

        if (stats.curHealth <= 0)
        {
            GameMaster.KillPlayer(this);
        }
        

    }

    void EndInvincible()
    {
        invincible = false;
    }
    
    void StartInvincible()
    {
        invincible = true;
    }

}
