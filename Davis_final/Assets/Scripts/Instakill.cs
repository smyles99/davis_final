﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instakill : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(new Vector2(0, 60) * Time.deltaTime);
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject); //Destroy pickup
            GameObject pistol = GameObject.Find("Pistol");//Reference script on game object pistol
            Weapon weapon = pistol.GetComponent<Weapon>();
            weapon.Damage = 99999;//Increase damage
            weapon.Invoke("Pickup", 7f);//Destroy after 7 seconds
        }
       
        
    }

    void Pickup()
    {
        GameObject pistol = GameObject.Find("Pistol");
        Weapon weapon = pistol.GetComponent<Weapon>();
        weapon.Damage = 20;
    }

}
